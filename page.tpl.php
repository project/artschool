<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head><title><?php print $head_title; ?></title>
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<?php print $head; ?>
<?php print $styles; ?>
<?php print $scripts; ?>
</head>
<body>

<div id="header-bg"><img src="<?php print base_path(). path_to_theme() ?>/images/shim.gif" width="0" height="202" alt="shim" /></div>

<div id="wrap-main">

<div id="header-main">
<?php if ($breadcrumb): ?><div id="breadcrumb"><?php print $breadcrumb; ?></div><?php endif; ?>
<?php if ($mission): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>

<?php if ($title): ?><h1 class="title"><?php print $title; ?></h1><?php endif; ?>
<?php if ($tabs): ?><div class="tabs"><?php print $tabs; ?></div><?php endif; ?>
<?php if ($messages): print $messages; endif; ?>
<?php if ($help): print $help; endif; ?>
</div>


<div id="left-main">

<div id="content">
<?php print $content; ?>
</div>

</div><!-- end left-main -->
<div id="right-main">

<?php print $left; ?>
<?php print $right; ?>

</div><!-- end right-main -->
<div class="clearing"><img src="<?php print base_path(). path_to_theme() ?>/images/shim.gif" width="0" height="0" alt="shim" /></div><!-- end clearing -->
</div><!-- end wrap-main -->

<div id="wrap-footer">
<div id="left-footer">

<?php print $footer_left; ?>

</div><!-- end left-footer -->
<div id="center-footer">

<?php print $footer_center; ?>

</div><!-- end center-footer -->
<div id="right-footer">

<?php print $footer_right; ?>

</div><!-- end right-footer -->
<div class="clearing"><img src="<?php print base_path(). path_to_theme() ?>/images/shim.gif" width="0" height="0" alt="shim" /></div><!-- end clearing -->
</div><!-- end wrap-footer -->

<!-- footer -->
<div class="footer"><?php print $footer; ?></div>
<!-- end footer -->

<!-- copyright -->
<div class="copyright">
Design by <a href="http://nocstudio81.com/">NocStudio81.com</a><br />
Images from <a href="http://sxc.hu/">stock.xchng</a><br />
Powered by <a href="http://drupal.org/">Drupal</a><br />
Licensed Under the <a href="http://www.gnu.org/copyleft/gpl.html">GNU GPL</a><br />
Valid <a href="http://validator.w3.org/check?uri=referer">XHTML 1.0 Strict</a> &amp; <a href="http://jigsaw.w3.org/css-validator/check/referer">CSS</a>
</div>
<!-- end copyright -->

<div id="footer-bg"><img src="<?php print base_path(). path_to_theme() ?>/images/shim.gif" width="0" height="202" alt="shim" /></div>

<?php print $closure; ?>
</body>
</html>